export const getQueryString = (url, param) => {
  if (!url) return;
  if (typeof (url) !== 'string') return;
  const queryParams = url.split('?')[1];
  return new URLSearchParams(queryParams).get(param);
}
