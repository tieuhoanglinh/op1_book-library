import Vue from "vue";
import Router from "vue-router";
import BookListing from "./pages/BookListing";

Vue.use(Router);

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Book listing",
      component: BookListing
    },
  ]
});
