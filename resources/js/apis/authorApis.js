import { axiosInstance as axios } from "../app";
import { AUTHOR_ENDPOINT, DEFAULT_GET_ALL_PAGE_SIZE, RequestMethod } from "../constants";

export const getAuthorList = () => axios({
    url: AUTHOR_ENDPOINT,
    method: RequestMethod.GET,
    params: {
        per_page: DEFAULT_GET_ALL_PAGE_SIZE
    }
});
