import { axiosInstance as axios } from "../app";
import { LIBRARY_ENDPOINT, DEFAULT_GET_ALL_PAGE_SIZE, RequestMethod } from "../constants";

export const getLibraryList = () => axios({
    url: LIBRARY_ENDPOINT,
    method: RequestMethod.GET,
    params: {
        per_page: DEFAULT_GET_ALL_PAGE_SIZE
    }
});
