import {axiosInstance as axios} from "../app";
import {BOOK_ENDPOINT, DEFAULT_PAGE_SIZE, RequestMethod} from "../constants";
import {getQueryString} from "../utils";

export const getBookList = (url = BOOK_ENDPOINT, query) => {
  const page = getQueryString(url, 'page');
  const params = {
    per_page: DEFAULT_PAGE_SIZE,
    page: page || 1,
    ...query
  }
  return axios({url, method: RequestMethod.GET, params})
}

export const getBook = (id) => axios({url: `${BOOK_ENDPOINT}/${id}`, method: RequestMethod.GET})

export const createBook = (data) => axios({url: BOOK_ENDPOINT, method: RequestMethod.POST, data})

export const editBook = (data) => axios({url: `${BOOK_ENDPOINT}/${data.id}`, method: RequestMethod.PUT, data})

export const deleteBook = id => axios({url: `${BOOK_ENDPOINT}/${id}`, method: RequestMethod.DELETE})
