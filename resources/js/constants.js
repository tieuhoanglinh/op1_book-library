export const BOOK_ENDPOINT = '/api/v1/book';
export const AUTHOR_ENDPOINT = '/api/v1/author';
export const LIBRARY_ENDPOINT = '/api/v1/library';
export const DEFAULT_PAGE_SIZE = 5;
export const DEFAULT_GET_ALL_PAGE_SIZE = 999;
export const DEFAULT_ALERT_TIMEOUT = 1.5 * 1000;

export const RequestMethod = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  PATCH: 'PATCH',
  DELETE: 'DELETE'
}

export const INITIAL_AUTHOR = {
  id: 0,
  name: "",
  birth_date: "",
  genre: ""
}

export const INITIAL_LIBRARY = {
  name: "",
  address: ""
}

export const INITIAL_BOOK = {
  name: "",
  year: "",
  author: INITIAL_AUTHOR,
  library: []
}

export const INITIAL_FILTER = {
  "search[name]": "",
  "search[from_year]": "",
  "search[to_year]": "",
  "search[author][name]": "",
  "search[author][genre]": "",
  "search[library][name]": "",
}
