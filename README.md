<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

# Book Library
## Run docker
      docker-compose up
## API Documentation (Available after docker is up)
      http://localhost:3000/api/documentation
## Summary
This project is a book management system. There are 3 objects: book, author and library.
The goal of this coding project is to create a web interface through which a user can upload Books, Author, and Library data to the cloud and be able to easily retrieve and search / filter for it.

## Technical
 This project was developed based on MVC design pattern and RestfulAPI
 
 ### Backend
 Backend was developed using Laravel(PHP). 
 HTTP Requests will call into routes defined in routes/api.php.
 Client will get responses in JSON format.
 
 #### Controllers
 Controllers is used to handle and response requests. Can be found at app\Http\Controllers
 ##### BookController
    CreateController:create() extends BookController
    UpdateController:update() extends BookController
    IndexController:index() extends BookController
 	ViewController:view() extends BookController
 	DeleteController:delete() extends BookController
 	
 ##### AuthorController
    IndexController:index() extended AuthorController
    
 ##### LibraryController
    IndexController:index() extends LibraryController
    
 #### Models
    Books
    Authors
    Libraries
    BookLibraries
    
 #### Resources
 Can be found at App\Http\Resource. Used to transform models
    
 ### Frontend
 Frontend was developed using Vue(mounted directly into Laravel). Every web routes will be redirected to index, where the Vue App is mounted. Vue will take over from there.
 Promise based HTTP client via axios. Everything else is pretty basic since it's a very lightweight Vue Application
 
 
 
 
