<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class LibraryUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('libraries', function (Blueprint $table) {
            DB::statement('ALTER TABLE libraries ADD FULLTEXT `name` (`name`)');
            DB::statement('ALTER TABLE libraries ADD FULLTEXT `address` (`address`)');
            DB::statement('ALTER TABLE libraries ENGINE = InnoDB'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('libraries', function (Blueprint $table) {
            DB::statement('ALTER TABLE libraries DROP INDEX name');
            DB::statement('ALTER TABLE libraries DROP INDEX address');
        });
    }
}
