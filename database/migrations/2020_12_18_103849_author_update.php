<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AuthorUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('authors', function (Blueprint $table) {
            DB::statement('ALTER TABLE authors ADD FULLTEXT `name` (`name`)');
            DB::statement('ALTER TABLE authors ADD FULLTEXT `genre` (`genre`)');
            DB::statement('ALTER TABLE authors ENGINE = InnoDB'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('authors', function (Blueprint $table) {
            DB::statement('ALTER TABLE authors DROP INDEX name');
            DB::statement('ALTER TABLE authors DROP INDEX genre');
        });
    }
}
