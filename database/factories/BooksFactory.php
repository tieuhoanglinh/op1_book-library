<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Authors;
use App\Models\Books;
use App\Models\Libraries;
use Faker\Generator as Faker;

$factory->define(Books::class, function (Faker $faker) {
    return [
        'name' => $faker->words(3, true),
        'year' => $faker->year(),
        'author_id' => function() {
            return factory(Authors::class)->create()->id;
        }
    ];
});
