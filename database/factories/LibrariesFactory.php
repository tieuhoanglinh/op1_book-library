<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Libraries;
use Faker\Generator as Faker;

$factory->define(Libraries::class, function (Faker $faker) {
    return [
        'name' => $faker->company(),
        'address' => $faker->address(),
    ];
});
