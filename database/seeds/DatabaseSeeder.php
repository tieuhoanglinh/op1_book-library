<?php

use App\Models\Authors;
use App\Models\Books;
use App\Models\Libraries;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Books::class, 10)->create()->each(function($book) {
            $author = factory(Authors::class)->create();
            $book->author_id = $author->id;
            $libraries = factory(Libraries::class,2)->create();
            foreach($libraries as $library) {
                $book->libraries()->save($library);
            }           
        });
    }
}
