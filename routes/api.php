<?php

use App\Models\Books;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'v1'
], function() {

    Route::group([
        'namespace' => 'Book'
    ], function() {

        Route::get('/book', 'IndexController@index');
        Route::get('/book/{id}', 'ViewController@view');
        Route::post('/book', 'CreateController@create');
        Route::put('/book/{id}', 'UpdateController@update');
        Route::delete('/book/{id}', 'DeleteController@delete');
        Route::get('/search', 'SearchController@search');

        Route::get('/prepare', 'SearchController@prepare');
        Route::delete('/delete', 'SearchController@deleteIndex');
    });

    Route::group([
        'namespace' => 'Author'
    ], function() {
        Route::get('/author', 'IndexController@index');
    });

    Route::group([
        'namespace' => 'Library'
    ], function() {
        Route::get('/library', 'IndexController@index');
    });

});
