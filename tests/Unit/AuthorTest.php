<?php

namespace Tests\Unit;

use App\Models\Authors;
use App\Models\Books;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AuthorTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        DB::table("authors")->truncate();
    }

    /**
     * test list author with per_page and page
     */
    public function testIndex1()
    {
        $param = [
            "per_page" => 2,
            "page" => 3
        ];
        $author = factory(Authors::class, 20)->create();
        $this->partialMock(Request::class, function ($mock) use ($param) {
            $mock->shouldReceive('all')->once()->andReturn($param);
        });
        $response = $this->getJson('/api/v1/author', $param);

        $response->assertStatus(200)->assertJsonStructure([
            "data" => [
                [
                    "id", "name", "birth_date", "genre"
                ]
            ]
        ])->assertJsonCount(2, 'data')->assertJsonPath("meta.per_page", 2)->assertJsonPath("meta.last_page", 10)->assertJsonPath("meta.current_page", 3);

        $this->assertDatabaseCount('authors', 20);
    }

    /**
     * test list author with no record
     */
    public function testIndex2()
    {
        $param = [
            "per_page" => 2,
            "page" => 3
        ];
        // $author = factory(Authors::class,20)->create();
        // $this->partialMock(Request::class, function($mock) use ($param) {
        //     $mock->shouldReceive('all')->once()->andReturn($param);
        // });
        $response = $this->getJson('/api/v1/author', $param);

        $response->assertStatus(200)->assertJsonCount(0, 'data');

        $this->assertDatabaseCount('authors', 0);
    }

    /**
     * test list author with search
     */
    public function testIndex3()
    {
        $param = [
            "search" => [
                "name" => "Jack",
                "genre" => "fantasy"
            ]
        ];
        factory(Authors::class, 20)->create();
        factory(Authors::class, 5)->create()->each(function ($author) {
            $author->name = "Jack Hamiton";
            $author->genre = "fantasy";
            $author->save();
        });
        $this->partialMock(Request::class, function ($mock) use ($param) {
            $mock->shouldReceive('all')->once()->andReturn($param);
        });
        $response = $this->getJson('/api/v1/author', $param);

        $response->assertStatus(200)->assertJsonCount(5, 'data');

        $this->assertDatabaseCount('authors', 25);
    }
}
