<?php

namespace Tests\Unit;

use App\Models\Libraries;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class LibraryTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown():void
    {
        DB::table("libraries")->truncate();
    }

    /**
     * test list Library with per_page and page
     */
    public function testIndex1(){
        $param = [
            "per_page" => 2,
            "page" => 3
        ];
        $author = factory(Libraries::class,20)->create();
        $this->partialMock(Request::class, function($mock) use ($param) {
            $mock->shouldReceive('all')->once()->andReturn($param);
        });
        $response = $this->getJson('/api/v1/library', $param);

        $response->assertStatus(200)->assertJsonStructure([
            "data" => [
                [
                    "id", "name", "address"
                ]
            ]
        ])->assertJsonCount(2, 'data')->assertJsonPath("meta.per_page",2)->assertJsonPath("meta.last_page",10)->assertJsonPath("meta.current_page",3);

        $this->assertDatabaseCount('libraries', 20);

        
    }

    /**
     * test list Library with no record
     */
    public function testIndex2(){
        $param = [
            "per_page" => 2,
            "page" => 3
        ];
        // $author = factory(Libraries::class,20)->create();
        // $this->partialMock(Request::class, function($mock) use ($param) {
        //     $mock->shouldReceive('all')->once()->andReturn($param);
        // });
        $response = $this->getJson('/api/v1/library', $param);

        $response->assertStatus(200)->assertJsonCount(0, 'data');

        $this->assertDatabaseCount('libraries', 0);

        
    }

    /**
     * test list library with search
     */
    public function testIndex3()
    {
        $param = [
            "search" => [
                "name" => "Floria",
                "address" => "14 Silicon"
            ]
        ];
        factory(Libraries::class, 20)->create();
        factory(Libraries::class, 5)->create()->each(function ($library) {
            $library->name = "Floria Sea library";
            $library->address = "14 Silicon New Jessy NY";
            $library->save();
        });
        $this->partialMock(Request::class, function ($mock) use ($param) {
            $mock->shouldReceive('all')->once()->andReturn($param);
        });
        $response = $this->getJson('/api/v1/library', $param);

        $response->assertStatus(200)->assertJsonCount(5, 'data');

        $this->assertDatabaseCount('libraries', 25);
    }

}
