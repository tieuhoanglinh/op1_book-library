<?php

namespace Tests\Unit;

use App\Models\Authors;
use App\Models\Books;
use App\Models\Libraries;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class BookTest extends TestCase
{
    // use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    // public function testExample()
    // {
    //     $this->assertTrue(true);
    // }

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown():void
    {
        DB::table("books")->truncate();
        DB::table("authors")->truncate();
        DB::table("libraries")->truncate();
        DB::table("book_libraries")->truncate();
    }

    /**
     * Create test
     */

    /**
     * testCreate1
     * @tilte test with correct params
     */

    public function testCreate1()
    {
        $response = $this->postJson('/api/v1/book', [
            "name" => "Mathemathic Theory",
            "year" => "2018",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "name" =>  "Amazon",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', 'Create Successful!');
    }

    /**
     * Test create with missing param author
     */
    public function testCreate2()
    {
        $response = $this->postJson('/api/v1/book', [
            "name" => "Mathemathic Theory2",
            "year" => "2018",
            "author" => [
                // "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "name" =>  "Amazon",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response->assertStatus(400)->assertJson([
            'message' => 'Data author name, birthdate, genre is required!'
        ]);
    }

    /**
     * Test create with missing param name
     */
    public function testCreate3()
    {
        $response = $this->postJson('/api/v1/book', [
            // "name" => "Mathemathic Theory2",
            // "year" => "2018",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "name" =>  "Amazon",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response->assertStatus(400)->assertJson([
            'message' => 'The given data was invalid.'
        ]);
    }

    /**
     * Test create with checking have in database
     */
    public function testCreate4()
    {
        $response = $this->postJson('/api/v1/book', [
            "name" => "Mathemathic Theory5",
            "year" => "2019",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "name" =>  "Amazon",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', 'Create Successful!');

        $this->assertDatabaseHas('books', [
            "name" => "Mathemathic Theory5",
            "year" => "2019",
        ])->assertDatabaseHas('authors', [
            "name" =>  "John Snow",
            "birth_date" => "03/02/1970",
            "genre" => "Math"
        ])->assertDatabaseHas('libraries', [
            "name" =>  "Amazon",
            "address" => "4 White Road LA"
        ]);
    }

    /**
     * Test create with already have author
     */
    public function testCreate5()
    {
        $author = factory(Authors::class)->create();

        $response = $this->postJson('/api/v1/book', [
            "name" => "Mathemathic Theory7",
            "year" => "2019",
            "author" => [
                "id" =>  $author->id
            ],
            "library" => [
                [
                    "name" =>  "Amazon",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', 'Create Successful!');

        $this->assertDatabaseHas('books', [
            "name" => "Mathemathic Theory7",
            "year" => "2019",
        ])->assertDatabaseHas('libraries', [
            "name" =>  "Amazon",
            "address" => "4 White Road LA"
        ]);
    }

    /**
     * Test create with already have author but author not exist
     */
    public function testCreate6()
    {

        $response = $this->postJson('/api/v1/book', [
            "name" => "Mathemathic Theory7",
            "year" => "2019",
            "author" => [
                "id" =>  100
            ],
            "library" => [
                [
                    "name" =>  "Amazon",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response->assertStatus(400)->assertJson([
            'message' => 'This author is not exist'
        ]);
    }

    /**
     * Test create with already have library
     */
    public function testCreate7()
    {
        $library = factory(Libraries::class)->create();

        $response = $this->postJson('/api/v1/book', [
            "name" => "Mathemathic Theory8",
            "year" => "2019",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "id" => $library->id
                ]
            ]
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', 'Create Successful!');

        $this->assertDatabaseHas('books', [
            "name" => "Mathemathic Theory8",
            "year" => "2019",
        ]);
    }

    /**
     * Test create with already have library but library not exist
     */
    public function testCreate8()
    {

        $response = $this->postJson('/api/v1/book', [
            "name" => "Mathemathic Theory7",
            "year" => "2019",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "id" =>  100
                ]
            ]
        ]);

        $response->assertStatus(400)->assertJson([
            'message' => 'This library is not exist!'
        ]);
    }

    /**
     * Test create with missing library
     */
    public function testCreate9()
    {

        $response = $this->postJson('/api/v1/book', [
            "name" => "Mathemathic Theory10",
            "year" => "2019",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ]
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', 'Create Successful!');

        $this->assertDatabaseHas('books', [
            "name" => "Mathemathic Theory10",
            "year" => "2019",
        ]);
    }

    /**
     * Test create with missing library name
     */
    public function testCreate10()
    {

        $response = $this->postJson('/api/v1/book', [
            "name" => "Mathemathic Theory10",
            "year" => "2019",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response->assertStatus(400)->assertJson([
            'message' => 'Data library name, address is required!'
        ]);
    }


    /**
     * Index test
     */

    /**
     * test with normal index
     */
    public function testIndex1()
    {
        $book = factory(Books::class)->create(
            [
                "name" => "Mathemathic Theory3",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );
        $book->libraries()->createMany(factory(Libraries::class, 3)->make()->toArray());
        $response = $this->getJson('/api/v1/book', []);

        $response->assertStatus(200)->assertJsonStructure([
            "data" => [
                [
                    "id", "name", "year"
                ]
            ]
        ])->assertJsonCount(1, 'data');

        $this->assertDatabaseCount('books', 1);
    }

    /**
     * test index with don't have any records
     */
    public function testIndex2()
    {
        $response = $this->getJson('/api/v1/book', []);

        $response->assertStatus(200)->assertJsonStructure([]);

        $this->assertDatabaseCount('books', 0);
    }

    /**
     * Test index with have limit
     */
    public function testIndex3()
    {
        $param = [
            "per_page" => 2,
            "page" => 3
        ];
        $book = factory(Books::class, 20)->create();
        $this->partialMock(Request::class, function($mock) use ($param) {
            $mock->shouldReceive('all')->once()->andReturn($param);
        });
        $response = $this->getJson('/api/v1/book', $param);
        $response->assertStatus(200)->assertJsonStructure([
            "data" => [
                [
                    "id", "name", "year"
                ]
            ]
        ])->assertJsonCount(2, 'data')
        ->assertJsonPath("meta.per_page",2)->assertJsonPath("meta.last_page",10)->assertJsonPath("meta.current_page",3);

        $this->assertDatabaseCount('books', 20);
    }

    /**
     * Test index with search name exist
     */
    public function testIndex4()
    {
        $param = [
            "per_page" => 1,
            "search" => [
                "name" => "Mathemathic index"
            ]
        ];
        $author = factory(Authors::class)->create([
            "name" => "John Snow",
            "birth_date" => "03/02/1970",
            "genre" => "Math",
        ]);
        $book = factory(Books::class)->create(
            [
                "name" => "Mathemathic index",
                "year" => "2018",
                "author_id" => $author->id
            ]
        );
        $this->partialMock(Request::class, function($mock) use ($param) {
            $mock->shouldReceive('all')->once()->andReturn($param);
        });
        $response = $this->getJson('/api/v1/book',$param);
        $response->assertStatus(200)->assertJsonCount(1, 'data');

        $this->assertDatabaseCount('books', 1);
    }

    /**
     * Test index with search name is not exist
     */
    public function testIndex5()
    {
        $param = [
            "search" => [
                "name" => "Theo"
            ]
        ];
        $book = factory(Books::class)->create(
            [
                "name" => "Mathemathic Theory9",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );
        $this->partialMock(Request::class, function($mock) use ($param) {
            $mock->shouldReceive('all')->once()->andReturn($param);
        });
        $response = $this->getJson('/api/v1/book', $param);
        $response->assertStatus(200)->assertJsonCount(0, 'data');

        $this->assertDatabaseCount('books', 1);
    }

    /**
     * Test index with search author name 
     */
    public function testIndex6()
    {
        $param = [
            "search" => [
                "author" =>[
                    "name" => "Leonado",
                    "genre" => "sport"
                ]             
            ]
        ];
        $author = factory(Authors::class)->create([
            "name" => "Leonado Costa",
            "birth_date" => "03/02/1970",
            "genre" => "sport",
        ]);
        factory(Books::class,10)->create();
        factory(Books::class,5)->create()->each(function($book) use($author) {
            $book->author_id = $author->id;
            $book->save();
        });
        $this->partialMock(Request::class, function($mock) use ($param) {
            $mock->shouldReceive('all')->once()->andReturn($param);
        });
        $response = $this->getJson('/api/v1/book', $param);
        $response->assertStatus(200)->assertJsonCount(5, 'data');

        $this->assertDatabaseCount('books', 15);
    }

    /**
     * Test index with search Library name 
     */
    public function testIndex7()
    {
        $param = [
            "search" => [
                "library" =>[
                    "name" => "Newyork",
                    "address" => "5 lavender"
                ]             
            ]
        ];
        $library = factory(Libraries::class)->create([
            "name" => "Newyork Library",
            "address" => "5 lavender road NY",
        ]);
        factory(Books::class,10)->create();        
        $books = factory(Books::class,5)->create()->each(function($book) use($library) {
            $book->libraries()->save($library);
        });
        $this->partialMock(Request::class, function($mock) use ($param) {
            $mock->shouldReceive('all')->once()->andReturn($param);
        });
        $response = $this->getJson('/api/v1/book', $param);
        $response->assertStatus(200)->assertJsonCount(5, 'data');

        $this->assertDatabaseCount('books', 15);
    }





    /**
     * View test
     */

    /**
     * Test view with has record
     */
    public function testView1()
    {
        $book = factory(Books::class)->create(
            [
                "name" => "Mathemathic Theory5",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );
        $book->libraries()->createMany(factory(Libraries::class, 3)->make()->toArray());
        $response = $this->getJson('/api/v1/book/' . $book->id, []);
        $response->assertStatus(200)->assertJsonPath('data.name', 'Mathemathic Theory5');
    }

    /**
     * Test view with don't hasrecord
     */
    public function testView2()
    {
        $response = $this->getJson('/api/v1/book/25', []);
        $response->assertStatus(400)->assertJson([
            'message' => "This book isn't exist!"
        ]);
    }

    /**
     * Edit test
     */

    /**
     * Edit book has record and change library
     * expect old library don't has book which is soft delete
     */
    public function testEdit1()
    {
        $book = factory(Books::class)->create(
            [
                "name" => "Mathemathic Theory3",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );
        $library = factory(Libraries::class)->create(
            [
                "name" =>  "Lazada",
                "address" => "4 White Road LA"
            ]
        );
        $book->libraries()->save($library);
        $response = $this->putJson('/api/v1/book/' . $book->id, [
            "name" => "Mathemathic Logic2",
            "year" => "2018",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "name" =>  "Amazon3",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response->assertStatus(200)->assertJson(['message' => 'Update Successfull!']);

        $this->assertDatabaseHas('books', [
            "name" => "Mathemathic Logic2",
            "year" => "2018",
        ])->assertDatabaseHas('authors', [
            "name" =>  "John Snow",
            "birth_date" => "03/02/1970",
            "genre" => "Math"
        ])->assertDatabaseHas('libraries', [
            "name" =>  "Amazon3",
            "address" => "4 White Road LA"
        ])->assertSoftDeleted('libraries', [
            "name" =>  "Lazada",
            "address" => "4 White Road LA"
        ]);
    }

    /**
     * edit record with has library and change library
     * expect library still has book which don't soft delete
     */
    public function testEdit2()
    {
        $book_1 = factory(Books::class)->create(
            [
                "name" => "Edit Theory5",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );

        $author = factory(Authors::class)->create();

        $book_2 = factory(Books::class)->create(
            [
                "name" => "Update Theory5",
                "year" => "2019",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );
        $library = factory(Libraries::class)->create(
            [
                "name" =>  "Lazada2",
                "address" => "4 White Road LA"
            ]
        );
        $book_1->libraries()->save($library);
        $book_2->libraries()->save($library);
        $response = $this->putJson('/api/v1/book/' . $book_1->id, [
            "name" => "Mathemathic Logic9",
            "year" => "2019",
            "author" => [
                "id" => $author->id
            ],
            "library" => [
                [
                    "name" =>  "Amazon1",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response->assertStatus(200)->assertJson(['message' => 'Update Successfull!']);

        $this->assertDatabaseHas('books', [
            "name" => "Mathemathic Logic9",
            "year" => "2019",
        ])->assertDatabaseHas('authors', [
            "name" =>  "John Snow",
            "birth_date" => "03/02/1970",
            "genre" => "Math"
        ])->assertDatabaseHas('libraries', [
            "name" =>  "Amazon1",
            "address" => "4 White Road LA"
        ])->assertDatabaseHas('libraries', [
            "name" =>  "Lazada2",
            "address" => "4 White Road LA"
        ]);
    }

    /**
     * Edit book has record and change library to a library isn't exist
     */
    public function testEdit3()
    {
        $book = factory(Books::class)->create(
            [
                "name" => "Mathemathic Theory3",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );
        $library = factory(Libraries::class)->create(
            [
                "name" =>  "Lazada",
                "address" => "4 White Road LA"
            ]
        );
        $book->libraries()->save($library);
        $response = $this->putJson('/api/v1/book/' . $book->id, [
            "name" => "Mathemathic Logic2",
            "year" => "2018",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "id" => 100,
                ]
            ]
        ]);

        $response->assertStatus(400)->assertJson(['message' => 'This library is not exist!']);
    }

    /**
     * Edit book has record and change library to a library missing param name
     */
    public function testEdit4()
    {
        $book = factory(Books::class)->create(
            [
                "name" => "Mathemathic Theory3",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );
        $library = factory(Libraries::class)->create(
            [
                "name" =>  "Lazada",
                "address" => "4 White Road LA"
            ]
        );
        $book->libraries()->save($library);
        $response = $this->putJson('/api/v1/book/' . $book->id, [
            "name" => "Mathemathic Logic2",
            "year" => "2018",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "address" => "9 Moutain View LA"
                ]
            ]
        ]);

        $response->assertStatus(400)->assertJson(['message' => 'Data library name, address is required!']);
    }

    /**
     * Test edit with record author is not exist
     */
    public function testEdit5()
    {
        $book = factory(Books::class)->create(
            [
                "name" => "Mathemathic Theory3",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );

        $response = $this->putJson('/api/v1/book/' . $book->id, [
            "name" => "Mathemathic Logic2",
            "year" => "2018",
            "author" => [
                "id" => 100
            ],
            "library" => [
                [
                    "name" =>  "Amazon3",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response->assertStatus(400)->assertJson(['message' => 'This author is not exist']);

    }

    /**
     * Test edit with record author is missing param name
     */
    public function testEdit6()
    {
        $book = factory(Books::class)->create(
            [
                "name" => "Mathemathic Theory3",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );

        $response = $this->putJson('/api/v1/book/' . $book->id, [
            "name" => "Mathemathic Logic2",
            "year" => "2018",
            "author" => [
                "birth_date" => "03/02/1970",
                "genre" => "Math",
            ],
            "library" => [
                [
                    "name" =>  "Amazon3",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response->assertStatus(400)->assertJson(['message' => 'Data author name, birthdate, genre is required!']);
    }

    /**
     * edit test with don't has record
     */
    public function testEdit10()
    {
        $response = $this->putJson('/api/v1/book/40', [
            "name" => "Mathemathic Logic9",
            "year" => "2019",
            "author" => [
                "name" =>  "John Snow",
                "birth_date" => "03/02/1970",
                "genre" => "Math"
            ],
            "library" => [
                [
                    "name" =>  "Amazon1",
                    "address" => "4 White Road LA"
                ]
            ]
        ]);

        $response->assertStatus(400)->assertJson(['message' => 'This Book is not exist!']);
    }

    /**
     * Delete test
     */

    /**
     * Delete record is not exist
     */
    public function testDelete1()
    {

        $response = $this->deleteJson('/api/v1/book/40', []);
        $response->assertStatus(400)->assertJson([
            "message" => "This book isn't exist!",
        ]);
    }

    /**
     * delete record has a library
     * expect that library don't has any book which is soft delete
     */
    public function testDelete2()
    {
        $book = factory(Books::class)->create(
            [
                "name" => "Mathemathic Theory3",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );
        $libraries = factory(Libraries::class)->create([
            "name" => "Tiki",
            "address" => "4 White Road LA"
        ]);
        $book->libraries()->save($libraries);

        $response = $this->deleteJson('/api/v1/book/' . $book->id, []);
        $response->assertStatus(200)->assertJson([
            'message' => 'Delete Successfull!'
        ]);
        $this->assertSoftDeleted('books', [
            "name" => "Mathemathic Theory3",
            "year" => "2018"
        ])->assertSoftDeleted('libraries', [
            "name" => "Tiki",
            "address" => "4 White Road LA"
        ]);
    }

    /**
     * delete record has a library
     * expect that library has a book which isn't soft delete
     */
    public function testDelete3()
    {
        $book_1 = factory(Books::class)->create(
            [
                "name" => "Matrix",
                "year" => "2018",
                "author_id" => factory(Authors::class)->create([
                    "name" => "John Snow",
                    "birth_date" => "03/02/1970",
                    "genre" => "Math",
                ])
            ]
        );

        $book_2 = factory(Books::class)->create(
            [
                "name" => "Geographic",
                "year" => "2017",
                "author_id" => factory(Authors::class)->create([
                    "name" => "Kat Hun",
                    "birth_date" => "04/07/1979",
                    "genre" => "geo",
                ])
            ]
        );

        $libraries = factory(Libraries::class)->create([
            "name" => "Tiki1",
            "address" => "4 White Road LA"
        ]);
        $book_1->libraries()->save($libraries);
        $book_2->libraries()->save($libraries);

        $response = $this->deleteJson('/api/v1/book/' . $book_1->id, []);
        $response->assertStatus(200)->assertJson([
            'message' => 'Delete Successfull!'
        ]);
        $this->assertSoftDeleted('books', [
            "name" => "Matrix",
            "year" => "2018",
        ])->assertDatabaseHas('libraries', [
            "name" => "Tiki1",
            "address" => "4 White Road LA"
        ]);
    }
}
