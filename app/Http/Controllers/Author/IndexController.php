<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\Author\AuthorController;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class IndexController extends AuthorController
{
    /**
     * @OA\Get(
     *      path="/api/v1/author",
     *      operationId="getListAuthor",
     *      tags={"Author"},
     *      summary="Get List Author",
     *      description="Returns list Author data",
     *      @OA\RequestBody(
     *          required=false,
     *          @OA\JsonContent(ref="#/components/schemas/VListAuthorRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index(Request $request) {
        try {
            $data = $request->all();
            $query = $this->model->select('*');
            $limit = !empty($data['per_page'])? (int) $data['per_page'] : 10;
            $page = !empty($data['page'])? (int) $data['page'] : null;
            if(!empty($data['search'])) {
                $search = $data['search'];
                if(!empty($search['name'])) {
                    $query->whereRaw("MATCH (name) AGAINST (? IN NATURAL LANGUAGE MODE)", $this->fullTextWildcards($search['name']));
                }
                if(!empty($search['birth_date'])) {
                    $query->where('birth_date', 'like', '%'.$search['birth_date'].'%');
                }
                if(!empty($search['genre'])) {
                    $query->whereRaw("MATCH (genre) AGAINST (? IN NATURAL LANGUAGE MODE)", $this->fullTextWildcards($search['genre']));
                }
            }
            $authors = $query->paginate($limit,['*'],'page',$page);
            $resource = $this->resource;
            $authors = $resource::collection($authors);
            return $authors;
        } catch(Exception $e) {
            throw new HttpException(400, $e->getMessage());
        }
    }
}