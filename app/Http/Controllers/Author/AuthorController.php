<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthorResource;
use App\Models\Authors;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    protected $model;
    protected $resource;

    public function __construct(
        Authors $author
    ) {
        $this->model = $author;
        $this->resource = AuthorResource::class;
    }
}
