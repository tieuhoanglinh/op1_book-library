<?php

namespace App\Http\Controllers\Book;

use Illuminate\Http\Request;
use App\Http\Controllers\Book\BookController;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CreateController extends BookController
{
    /**
     * @OA\Post(
     *      path="/api/v1/book",
     *      operationId="createBook",
     *      tags={"Book"},
     *      summary="Create new Book",
     *      description="Create new Book",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/VStoreBookRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function create(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string',
                'year' => 'required|string',
                'author' => 'required',
                'author.id' => 'integer',
                'author.name' => 'string',
                'author.birth_date' => 'string',
                'author.genre' => 'string',
                'library.*.name' => 'string',
                'library.*.address' => 'string',
                'library.*.id' => 'integer',
            ]);
            $data = [
                'name' => $request->post('name'),
                'year' => $request->post('year')
            ];
            DB::beginTransaction();
            $author = $request->post('author');
            $authorId = $author['id'] ?? 0;
            if (empty($authorId)) {     
                if(empty($author['name']) || empty($author['birth_date']) || empty($author['genre'])) {
                    throw new HttpException(400, 'Data author name, birthdate, genre is required!');
                }          
                $dataAuthor = [
                    'name' => $author['name'],
                    'birth_date' => $author['birth_date'],
                    'genre' => $author['genre'],
                ];

                $authorId = $this->author::firstOrCreate($dataAuthor)->id;
            } else {
                $author = $this->author::find($authorId);
                if(empty($author)) {
                    throw new HttpException(400, 'This author is not exist');
                }
            }
            $data['author_id'] = $authorId;
            $book = $this->model::create($data);
            if (!empty($book->id)) {
                $libraries = $request->post('library');
                if (!empty($libraries)) {
                    foreach ($libraries as $key => $library) {
                        $libraryId = $library['id'] ?? null;
                        if (empty($libraryId)) {
                            if (empty($library['name']) || empty($library['address'])) {
                                throw new HttpException(400, 'Data library name, address is required!');
                            }
                            $dataLibrary = [
                                'name' => $library['name'],
                                'address' => $library['address'],
                            ];
                            $libraryId =  $this->library::firstOrCreate($dataLibrary)->id;
                        } else {
                            $library = $this->library::find($libraryId);
                            if(empty($library)) {
                                throw new HttpException(400, 'This library is not exist!');
                            }
                        }
                        $dataBookLibrary = [
                            'book_id' => $book->id,
                            'library_id' => $libraryId
                        ];
                        $this->book_library->create($dataBookLibrary);
                    }
                }
            }
            DB::commit();
            return [
                'message' => 'Create Successful!'
            ];
        } catch (ValidationException $ve) {
            DB::rollBack();
            throw new BadRequestHttpException($ve->getMessage());
        }
    }
}
