<?php

namespace App\Http\Controllers\Book;

use Illuminate\Http\Request;
use App\Http\Controllers\Book\BookController;
use Exception;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UpdateController extends BookController
{
    /**
     * @OA\Put(
     *      path="/api/v1/book/{id}",
     *      operationId="updateBook",
     *      tags={"Book"},
     *      summary="Update Book",
     *      description="Create new Book",
     *      @OA\Parameter(
     *          name="id",
     *          description="Book id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/VStoreBookRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string',
                'year' => 'required|string',
                'author' => 'required',
                'author.id' => 'integer',
                'author.name' => 'string',
                'author.birth_date' => 'string',
                'author.genre' => 'string',
                'library.*.name' => 'string',
                'library.*.address' => 'string',
                'library.*.id' => 'integer',
            ]);
            $book = $this->model::find($id);
            if (empty($book)) {
                throw new HttpException(400, 'This Book is not exist!');
            }

            DB::beginTransaction();
            $author = $request->post('author');
            $authorId = $author['id'] ?? 0;
            if (empty($authorId)) {     
                if(empty($author['name']) || empty($author['birth_date']) || empty($author['genre'])) {
                    throw new HttpException(400, 'Data author name, birthdate, genre is required!');
                }          
                $dataAuthor = [
                    'name' => $author['name'],
                    'birth_date' => $author['birth_date'],
                    'genre' => $author['genre'],
                ];

                $authorId = $this->author::firstOrCreate($dataAuthor)->id;
            } else {
                $author = $this->author::find($authorId);
                if(empty($author)) {
                    throw new HttpException(400, 'This author is not exist');
                }
            }
            $book->name = $request->post('name');
            $book->year = $request->post('year');
            $book->author_id = $authorId;
            $book->save();
            $library_ids = $this->book_library->where("book_id",$book->id)->pluck('library_id');
            $this->book_library::where('book_id', $id)->delete();
            $libraries = $request->post('library');
            if (!empty($libraries)) {
                foreach ($libraries as $key => $library) {
                    $libraryId = $library['id'] ?? null;
                    if (empty($libraryId)) {
                        if (empty($library['name']) || empty($library['address'])) {
                            throw new HttpException(400, 'Data library name, address is required!');
                        }
                        $dataLibrary = [
                            'name' => $library['name'],
                            'address' => $library['address'],
                        ];
                        $libraryId =  $this->library::firstOrCreate($dataLibrary)->id;
                    } else {
                        $library = $this->library::find($libraryId);
                        if(empty($library)) {
                            throw new HttpException(400, 'This library is not exist!');
                        }
                    }
                    $dataBookLibrary = [
                        'book_id' => $book->id,
                        'library_id' => $libraryId
                    ];
                    $this->book_library->create($dataBookLibrary);
                }
            }
            $library_deletes = []; 
            foreach($library_ids as $library_id ) {
                $book_library = $this->book_library->where("library_id", $library_id)->first();
                if(empty($book_library)) {
                    $library_deletes[] = $library_id;
                }
            }
            $this->library->whereIn('id', $library_deletes)->delete();
            DB::commit();
            return [
                'message' => 'Update Successfull!'
            ];
        } catch (Exception $e) {
            DB::rollBack();
            throw new HttpException(400, $e->getMessage());
        }
    }
}
