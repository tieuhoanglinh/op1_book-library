<?php

namespace App\Http\Controllers\Book;

use Illuminate\Http\Request;
use App\Http\Controllers\Book\BookController;
use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ViewController extends BookController
{
    /**
     * @OA\Get(
     *      path="/api/v1/book/{id}",
     *      operationId="getBookData",
     *      tags={"Book"},
     *      summary="Get Data Book",
     *      description="Returns book data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Book id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function view(Request $request, $id)
    {

        try {
            $book = $this->model::with(['author','libraries'])->find($id);
            if (empty($book)) {
                throw new HttpException(400, "This book isn't exist!");
            }
            $resource = $this->resource;
            $book = $resource::make($book);
            return $book;
        } catch (Exception $e) {
            throw new HttpException(400, $e->getMessage());
        }
    }
}
