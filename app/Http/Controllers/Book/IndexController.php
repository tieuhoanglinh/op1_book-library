<?php

namespace App\Http\Controllers\Book;

use Illuminate\Http\Request;
use App\Http\Controllers\Book\BookController;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpKernel\Exception\HttpException;

class IndexController extends BookController
{
    /**
     * @OA\Get(
     *      path="/api/v1/book",
     *      operationId="getListBook",
     *      tags={"Book"},
     *      summary="Get List Book",
     *      description="Returns list book data",
     *      @OA\RequestBody(
     *          required=false,
     *          @OA\JsonContent(ref="#/components/schemas/VListBookRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *          
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index(Request $request)
    {
        try {
            $data = $request->all();
            $limit = !empty($data['per_page'])? (int) $data['per_page'] : 10;
            $page = !empty($data['page'])? (int) $data['page'] : null;
            $query = $this->model->with(['author','libraries']);
            if(!empty($data['search'])) {
                $search = $data['search'];
                if(!empty($search['name'])) {
                    // $query->where('name', 'like', '%'.$search['name'].'%');
                    $query->whereRaw("MATCH (name) AGAINST (? IN NATURAL LANGUAGE MODE)", $this->fullTextWildcards($search['name']));
                }

                if(!empty($search['from_year'])) {
                    $query->where('year','>=' ,$search['from_year']);
                }

                if(!empty($search['to_year'])) {
                    $query->where('year','<=' ,$search['to_year']);
                }

                if(!empty($search['author'])) {
                    if(!empty($search['author']['name'])) {
                        $query->whereHas('author', function (Builder $query) use($search) {
                            // $query->where('name', 'like', '%'.$search['author']['name'].'%');
                            $query->whereRaw("MATCH (name) AGAINST (? IN NATURAL LANGUAGE MODE)", $this->fullTextWildcards($search['author']['name']));
                        });
                    }
                    if(!empty($search['author']['birth_date'])) {
                        $query->whereHas('author', function (Builder $query) use($search) {
                            $query->where('birth_date', 'like', '%'.$search['author']['birth_date'].'%');
                        });
                    }

                    if(!empty($search['author']['genre'])) {
                        $query->whereHas('author', function (Builder $query) use($search) {
                            $query->whereRaw("MATCH (genre) AGAINST (? IN NATURAL LANGUAGE MODE)", $this->fullTextWildcards($search['author']['genre']));
                        });
                    }
                }

                if(!empty($search['library'])) {
                    if(!empty($search['library']['name'])) {
                        $query->whereHas('libraries', function (Builder $query) use($search) {
                            $query->whereRaw("MATCH (name) AGAINST (? IN NATURAL LANGUAGE MODE)", $this->fullTextWildcards($search['library']['name']));
                        });
                    }

                    if(!empty($search['library']['address'])) {
                        $query->whereHas('libraries', function (Builder $query) use($search) {
                            $query->whereRaw("MATCH (address) AGAINST (? IN NATURAL LANGUAGE MODE)", $this->fullTextWildcards($search['library']['address']));
                        });
                    }
                }

            }
            $books = $query->paginate($limit,['*'],'page',$page);
            $resource = $this->resource;
            $books = $resource::collection($books);
            return $books;
        } catch (Exception $e) {
            throw new HttpException(400, $e->getMessage());
        }
    }
}
