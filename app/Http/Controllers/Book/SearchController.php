<?php

namespace App\Http\Controllers\Book;

use Illuminate\Http\Request;
use App\Http\Controllers\Book\BookController;
use App\Models\Books;
use Elasticsearch\ClientBuilder;
use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SearchController extends BookController
{
    public function search(Request $request) {
        try {
            $keyword = $request->get('keyword');
            $client = ClientBuilder::create()->build();
            $params = [
                'index' => 'books',
                'body'  => [
                    'query' => [
                        'match' => [
                            'name' => $keyword
                        ]
                    ]
                ]
            ];
            
            $response = $client->search($params);
            return $response;
        } catch(Exception $e) {
            throw new HttpException(400, $e->getMessage());
        }
    }

    public function prepare() {
        try {
            $client = ClientBuilder::create()->build();

            /** create index */
            $params = [
                'index' => 'books',
                'body'  => [
                    'settings' => [
                        'number_of_shards' => 5,
                        'number_of_replicas' => 0
                    ]
                ]
            ];
            
            $client->indices()->create($params);

            $all = Books::all()->toArray();
            foreach($all as $value) {
                $params = [
                    'index' => 'books',
                    'id'    => $value['id'],
                    'body'  => $value
                ];
                $client->index($params);
            }
            return [
                'message' => 'Prepare Successfull!'
            ];

        } catch(Exception $e) {
            throw new HttpException(400, $e->getMessage());
        }
        
    }

    public function deleteIndex(Request $request) {
        try{
            $client = ClientBuilder::create()->build();
            $deleteParams = [
                'index' => 'books'
            ];
            $response = $client->indices()->delete($deleteParams);
            return [
                'message' => 'Delete Succesfull',
            ];
        } catch(Exception $e) {
            throw new HttpException(400,$e->getMessage());
        }   
    }
}