<?php

namespace App\Http\Controllers\Book;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BookResource;
use App\Models\Authors;
use App\Models\BookLibraries;
use App\Models\Books;
use App\Models\Libraries;
use App\Transformers\BookTransformer;
use League\Fractal\Manager;

class BookController extends Controller
{
    protected $model;
    protected $author;
    protected $library;
    protected $resource;
    protected $book_library;

    public function __construct(
        Books $book,
        Authors $author,
        Libraries $library,
        BookLibraries $book_library
    ) {
        $this->model = $book;
        $this->author = $author;
        $this->library = $library;
        $this->book_library = $book_library;
        $this->resource = BookResource::class;
    }
}
