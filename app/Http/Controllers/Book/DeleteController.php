<?php

namespace App\Http\Controllers\Book;

use Illuminate\Http\Request;
use App\Http\Controllers\Book\BookController;
use Exception;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;

class DeleteController extends BookController
{
    /**
     * @OA\Delete(
     *      path="/book/{id}",
     *      operationId="deleteBook",
     *      tags={"Book"},
     *      summary="Delete existing Book",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="Book id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function delete(Request $request, $id)
    {
        try {
            $book = $this->model::find($id);
            if (empty($book)) {
                throw new HttpException(400, "This book isn't exist!");
            }
            DB::beginTransaction();
            $library_ids = $this->book_library->where("book_id",$book->id)->pluck('library_id');
            $this->book_library->where("book_id",$book->id)->delete();
            $library_deletes = []; 
            foreach($library_ids as $library_id ) {
                $book_library = $this->book_library->where("library_id", $library_id)->first();
                if(empty($book_library)) {
                    $library_deletes[] = $library_id;
                }
            }
            $this->library->whereIn('id', $library_deletes)->delete();
            $book->delete();
            DB::commit();
            return [
                'message' => 'Delete Successfull!'
            ];
        } catch (Exception $e) {
            DB::rollBack();
            throw new HttpException(400, $e->getMessage());
        }
    }
}
