<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use App\Http\Resources\LibraryResource;
use App\Models\Libraries;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    protected $model;
    protected $resource;

    public function __construct(
        Libraries $libraries
    ) {
        $this->model = $libraries;
        $this->resource = LibraryResource::class;
    }
}
