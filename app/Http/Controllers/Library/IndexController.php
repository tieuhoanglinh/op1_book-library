<?php

namespace App\Http\Controllers\Library;

use App\Http\Resources\LibraryResource;
use App\Models\Libraries;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class IndexController extends LibraryController
{
    /**
     * @OA\Get(
     *      path="/api/v1/library",
     *      operationId="getListLibrary",
     *      tags={"Library"},
     *      summary="Get List Library",
     *      description="Returns list Library data",
     *      @OA\RequestBody(
     *          required=false,
     *          @OA\JsonContent(ref="#/components/schemas/VListLibraryRequest")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index(Request $request) {
        try {
            $data = $request->all();
            $query = $this->model->select('*');
            $limit = !empty($data['per_page'])? (int) $data['per_page'] : 10;
            $page = !empty($data['page'])? (int) $data['page'] : null;
            if(!empty($data['search'])) {
                $search = $data['search'];
                if(!empty($search['name'])) {
                    $query->whereRaw("MATCH (name) AGAINST (? IN NATURAL LANGUAGE MODE)", $this->fullTextWildcards($search['name']));
                }
                if(!empty($search['address'])) {
                    $query->whereRaw("MATCH (address) AGAINST (? IN NATURAL LANGUAGE MODE)", $this->fullTextWildcards($search['address']));
                }
            }
            $libraries = $query->paginate($limit,['*'],'page',$page);
            $resource = $this->resource;
            $libraries = $resource::collection($libraries);
            return $libraries;
        } catch(Exception $e) {
            throw new HttpException(400,$e->getMessage());
        }
    }
}