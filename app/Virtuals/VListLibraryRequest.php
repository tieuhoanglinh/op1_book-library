<?php

/**
 * @OA\Schema(
 *      title="List Library request",
 *      description="List Library request search data",
 *      type="object"
 * )
 */

class VListLibraryRequest
{
    /**
     * @OA\Property(
     *      title="Per page",
     *      description="Number records per page",
     *      example=5
     * )
     *
     * @var int
     */
    public $per_page;

    /**
     * @OA\Property(
     *      title="Page",
     *      description="The page number",
     *      example=2
     * )
     *
     * @var int
     */
    public $page;

    /**
     * @OA\Property(
     *      title="Search",
     *      description="Params for search",
     * )
     *
     * @var \App\Virtuals\Models\VLibrarySearch
     */
    public $search;
}