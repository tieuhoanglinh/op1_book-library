<?php

/**
 * @OA\Schema(
 *      title="List Author request",
 *      description="List Author request search data",
 *      type="object"
 * )
 */

class VListAuthorRequest
{
    /**
     * @OA\Property(
     *      title="Per page",
     *      description="Number records per page",
     *      example=5
     * )
     *
     * @var int
     */
    public $per_page;

    /**
     * @OA\Property(
     *      title="Page",
     *      description="The page number",
     *      example=2
     * )
     *
     * @var int
     */
    public $page;

    /**
     * @OA\Property(
     *      title="Search",
     *      description="Params for search",
     * )
     *
     * @var \App\Virtuals\Models\VAuthorSearch
     */
    public $search;
}