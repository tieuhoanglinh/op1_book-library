<?php

/**
 * @OA\Schema(
 *      title="Store Book request",
 *      description="Store Book request body data",
 *      type="object",
 *      required={"name","year","author"}
 * )
 */

class VStoreBookRequest
{
    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the new Book",
     *      example="Mathematic"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Year",
     *      description="Year of the book",
     *      example="2019"
     * )
     *
     * @var date('Y')
     */
    public $year;

    /**
     * @OA\Property(
     *      title="Author",
     *      description="Author data",
     *      format="int64",
     * )
     *
     * @var \App\Virtuals\Models\VAuthor
     */
    public $author;

    /**
     * @OA\Property(
     *      title="Author",
     *      description="Author data",
     *      format="int64",
     * )
     *
     * @var \App\Virtuals\Models\VLibrary[]
     */
    public $libraries;
}