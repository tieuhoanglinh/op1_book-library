<?php
namespace App\Virtuals\Models;
/**
 * @OA\Schema(
 *      title="VBook model",
 *      description="Book model",
 *     @OA\Xml(
 *         name="VBook"
 *     )
 * )
 */

class VBook
{
    /**
     * @OA\Property(
     *      title="Name",
     *      description="Search name of book",
     *      example="Mathematic"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Year",
     *      description="Search year of book",
     *      example="2016"
     * )
     *
     * @var date('Y')
     */
    public $year;
}