<?php
namespace App\Virtuals\Models;
/**
 * @OA\Schema(
 *      title="VAuthorSearch model",
 *      description="VAuthorSearch model",
 *     @OA\Xml(
 *         name="VAuthorSearch"
 *     )
 * )
 */

class VAuthorSearch
{
    
    /**
     * @OA\Property(
     *      title="Name",
     *      description="Search name of the Author",
     *      example="Charle Dicken"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Birthdate",
     *      description="Search birthdate",
     *      example="01/03/1987"
     * )
     *
     * @var string
     */
    public $birth_date;

    /**
     * @OA\Property(
     *      title="Genre",
     *      description="Search Genre",
     *      example="math"
     * )
     *
     * @var string
     */
    public $genre;
}