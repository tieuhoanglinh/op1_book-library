<?php
namespace App\Virtuals\Models;
/**
 * @OA\Schema(
 *      title="VAuthor model",
 *      description="Author model",
 *     @OA\Xml(
 *         name="VAuthor"
 *     )
 * )
 */

class VAuthor
{
    /**
     * @OA\Property(
     *      title="Id",
     *      description="Id of the Author",
     *      example=2
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      title="Name",
     *      description="Name of the Author",
     *      example="Charle Dicken"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Birthdate",
     *      description="Author birthdate",
     *      example="01/03/1987"
     * )
     *
     * @var string
     */
    public $birth_date;

    /**
     * @OA\Property(
     *      title="Genre",
     *      description="Author Genre",
     *      example="math"
     * )
     *
     * @var string
     */
    public $genre;
}