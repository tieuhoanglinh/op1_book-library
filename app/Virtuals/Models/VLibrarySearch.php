<?php
namespace App\Virtuals\Models;
/**
 * @OA\Schema(
 *      title="VLibrarySearch model",
 *      description="VLibrarySearch model",
 *     @OA\Xml(
 *         name="VLibrarySearch"
 *     )
 * )
 */

class VLibrarySearch
{
    /**
     * @OA\Property(
     *      title="Name",
     *      description="Search Name of the Library",
     *      example="Sea animal library"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Address",
     *      description="Search Address of the Library",
     *      example="3 northhill CA"
     * )
     *
     * @var string
     */
    public $address;
}