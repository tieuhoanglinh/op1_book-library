<?php
namespace App\Virtuals\Models;
/**
 * @OA\Schema(
 *      title="VLibrary model",
 *      description="Library model",
 *     @OA\Xml(
 *         name="VLibrary"
 *     )
 * )
 */

class VLibrary
{
    /**
     * @OA\Property(
     *      title="Id",
     *      description="Id of the Library",
     *      example=2
     * )
     *
     * @var int
     */
    public $id;

    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the Library",
     *      example="Sea animal library"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Address",
     *      description="Address of the Library",
     *      example="3 northhill CA"
     * )
     *
     * @var string
     */
    public $address;
}