<?php
namespace App\Virtuals\Models;
/**
 * @OA\Schema(
 *      title="VBookSearch model",
 *      description="Book search model",
 *     @OA\Xml(
 *         name="VBookSearch"
 *     )
 * )
 */

class VBookSearch
{
    /**
     * @OA\Property(
     *      title="Name",
     *      description="Search name of book",
     *      example="Mathematic"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="From Year",
     *      description="Search year of book from",
     *      example="2016"
     * )
     *
     * @var date('Y')
     */
    public $from_year;

    /**
     * @OA\Property(
     *      title="To Year",
     *      description="Search year of book to",
     *      example="2019"
     * )
     *
     * @var date('Y')
     */
    public $to_year;

    /**
     * @OA\Property(
     *      title="Author",
     *      description="Search field author",
     * )
     *
     * @var \App\Virtuals\Models\VAuthorSearch
     */
    public $author;

    /**
     * @OA\Property(
     *      title="Library",
     *      description="Search field Library",
     * )
     *
     * @var \App\Virtuals\Models\VLibrarySearch
     */
    public $library;
}