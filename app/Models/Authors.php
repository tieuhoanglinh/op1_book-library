<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Authors extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','birth_date','genre'];

    public function books()
    {
        return $this->hasMany('App\Models\Authors', 'author_id');
    }

}
