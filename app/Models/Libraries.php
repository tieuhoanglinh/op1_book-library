<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Libraries extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','address'];

    public function books() {
        return $this->belongsToMany('App\Models\Book','book_libraries', 'library_id', 'book_id');
    }

}
