<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Elasticsearch\ClientBuilder;

class Books extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'year', 'author_id'];

    public function author()
    {
        return $this->belongsTo('App\Models\Authors', 'author_id');
    }

    public function libraries()
    {
        return $this->belongsToMany('App\Models\Libraries', 'book_libraries', 'book_id', 'library_id');
    }

}
