<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookLibraries extends Model
{

    public $timestamps = false;

    protected $fillable = ['book_id','library_id'];
}
